package org.prac.philippines;

import java.util.List;

public class Restaurant {

	IHotDrink hotDrink;
	
	Restaurant(IHotDrink hotDrink)
	{
		this.hotDrink = hotDrink;
	}
	
	public void prepareHotDrink()
	{
		hotDrink.prepareHotDrink();
	}
	
	
	
	
	

}

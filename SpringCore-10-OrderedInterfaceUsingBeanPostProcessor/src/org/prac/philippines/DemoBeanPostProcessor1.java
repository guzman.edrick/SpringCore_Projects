package org.prac.philippines;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;

public class DemoBeanPostProcessor1 implements BeanPostProcessor, Ordered {

	public Object postProcessAfterInitialization(Object arg0, String arg1)
			throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("This is after string instantiates bean and before init cycle by " + arg1);
		return arg0;
	}

	public Object postProcessBeforeInitialization(Object arg0, String arg1)
			throws BeansException {
		
		System.out.println("This is after the init event " + arg1);

		// TODO Auto-generated method stub
		return arg0;
	}

	public int getOrder() {
		// TODO Auto-generated method stub
		return 1;
	}

}

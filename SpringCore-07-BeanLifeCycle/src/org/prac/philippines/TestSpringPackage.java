package org.prac.philippines;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpringPackage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("SpringConfig.xml");
		
		Restaurant restaurantObj = (Restaurant) context.getBean("restaurantBean");
		restaurantObj.setWelcomeNote("Hello restaurantObj");
		restaurantObj.greetCustomer();
		((AbstractApplicationContext)context).registerShutdownHook();
//		Restaurant restaurantObj2 = (Restaurant) context.getBean("restaurantBean");
//		restaurantObj2.greetCustomer();
//		
		
	
	}

}

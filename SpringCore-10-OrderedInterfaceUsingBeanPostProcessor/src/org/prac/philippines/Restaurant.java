package org.prac.philippines;

import java.util.List;

public class Restaurant {

	
	String welcomeNote;
	
	public void setWelcomeNote(String welcomeNote) {
		this.welcomeNote = welcomeNote;
	}
	public void greetCustomer()
	{
		System.out.println(welcomeNote);
	}
	
	public void init()
	{
		System.out.println("Restaurant has started");
	}
	
	public void destroy()
	{
		System.out.println("Restaurant has been destroyed");
	}
	
	
	

}
